module github.com/dbaux/api_test_automation

go 1.13

require (
	github.com/ajg/form v1.5.1
	github.com/buger/jsonparser v0.0.0-20191204142016-1a29609e0929
	github.com/davecgh/go-spew v1.1.1
	github.com/educlos/testrail v0.0.0-20190627213040-ca1b25409ae2
	github.com/fatih/structs v1.1.0
	github.com/google/go-querystring v1.0.0
	github.com/gorilla/websocket v1.4.1
	github.com/imkira/go-interpol v1.1.0
	github.com/klauspost/compress v1.9.4
	github.com/kr/pretty v0.2.0 // indirect
	github.com/pmezard/go-difflib v1.0.0
	github.com/satori/go.uuid v1.2.0
	github.com/sergi/go-diff v0.0.0-20140808132932-97b2266dfe4b
	github.com/stretchr/testify v1.4.0
	github.com/valyala/bytebufferpool v1.0.0
	github.com/valyala/fasthttp v0.0.0-20171207120941-e5f51c11919d
	github.com/xeipuuv/gojsonpointer v0.0.0-20190905194746-02993c407bfb
	github.com/xeipuuv/gojsonreference v0.0.0-20180127040603-bd5ef7bd5415
	github.com/xeipuuv/gojsonschema v1.2.0
	github.com/yalp/jsonpath v0.0.0-20180802001716-5cc68e5049a0
	github.com/yudai/gojsondiff v0.0.0-20170107030110-7b1b7adf999d
	github.com/yudai/golcs v0.0.0-20150405163532-d1c525dea8ce
	golang.org/x/net v0.0.0-20191209160850-c0dbc17a3553
	golang.org/x/text v0.3.2
	gopkg.in/yaml.v2 v2.2.7
	moul.io/http2curl v1.0.1-0.20190925090545-5cd742060b0e
)
