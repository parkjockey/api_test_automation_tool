package main

import (
	"path/filepath"
	"sync"

	"github.com/dbaux/api_test_automation/benchmark"
	"github.com/dbaux/api_test_automation/fastrequest"
	benchmodel "github.com/dbaux/api_test_automation/model/bench_config"
	perfmodel "github.com/dbaux/api_test_automation/model/perf_config"
	model "github.com/dbaux/api_test_automation/model/rest_config"
	"github.com/dbaux/api_test_automation/params"
	"github.com/dbaux/api_test_automation/parser"
	"github.com/dbaux/api_test_automation/util"
)

func main() {
	params.VarSession = make(map[string]string)
	abs, _ := filepath.Abs(params.PerfFileName)
	var configParser parser.Parser = parser.ConfigParser{PerfFileName: abs}
	var configsPerf []perfmodel.PerfConfig = configParser.ParsePerfConfiguration()
	var mainWg sync.WaitGroup
	for _, configPerf := range configsPerf {
		//fmt.Println("outside routine Perf Test for configured file-", configPerf.FileName)
		mainWg.Add(1)
		go func(configParser parser.Parser, configPerf perfmodel.PerfConfig) {
			//fmt.Println("Perf Test for configured file-", configPerf.ClientCountArr)
			var configRest []model.ConfigureRest = configParser.ParseConfiguration(configPerf.FileName)
			resultarr := make(map[int]*benchmodel.BenchResult)
			fastRest := fastrequest.FastRest{configParser, make(map[string]string), &sync.RWMutex{}, resultarr, 0}
			mybench := &benchmark.MyBench{}
			mybench = mybench.InitVar(configPerf.ClientCountArr, int64(configPerf.InitialTrafficTime), int64(configPerf.TrafficTimeStepUp), int64(configPerf.TrafficTimeMax), int64(configPerf.RequestsPerClientPerSec), configPerf.MaxConnPerHost, configPerf.MaxReadTimeout, configPerf.MaxWriteTimeout, configPerf.ResultPath)
			util.CreateFileWithHeader(configPerf.ResultPath)
			mybench.InitiateRequestAndBenchmark(configRest, &fastRest, configPerf.FileName, &mainWg, configPerf.TimeToReleaseConn)
		}(configParser, configPerf)
	}
	mainWg.Wait()
}
