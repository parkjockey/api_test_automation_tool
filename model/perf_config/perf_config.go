package perfmodel

type PerfConfigs struct {
	PerfConfigArr []PerfConfig
}

type PerfConfig struct {
	FileName                string `json:"fileName"`
	ClientCountArr          []int  `json:"clientCountArr"`
	InitialTrafficTime      int    `json:"intialTrafficTime"`
	TrafficTimeStepUp       int    `json:"trafficTimeStepUp"`
	TrafficTimeMax          int    `json:"trafficTimeMax"`
	RequestsPerClientPerSec int    `json:"requestsPerClientPerSec"`
	MaxConnPerHost          int    `json:"maxConnectionPerHost"`
	MaxReadTimeout          int    `json:"maxReadTimeout"`
	MaxWriteTimeout         int    `json:"maxWriteTimeout"`
	TimeToReleaseConn       int64  `json:"timeToReleaseConn"`
	ResultPath              string `json:"resultPath"`
}
