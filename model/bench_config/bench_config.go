package benchmodel

import "github.com/valyala/fasthttp"

type BenchConfiguration struct {
	Requests  int64
	Period    int64
	KeepAlive bool
	MyClient  fasthttp.Client
}

type BenchResult struct {
	ApiId          string
	TestRailId     int64
	ResponseStatus int
	ResponseTime   int64
	Response       string
	TestStatus     string
	FailureReason  string
	Description    string
}

const TestRailURL string = "https://reef.testrail.io"
