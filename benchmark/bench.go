package benchmark

import (
	"bufio"
	"bytes"
	"flag"
	"fmt"
	"io"
	"net"
	"os"
	"path/filepath"
	"runtime"
	"strconv"
	"strings"
	"sync"
	"sync/atomic"
	"time"

	"github.com/dbaux/api_test_automation/fastrequest"
	benchmodel "github.com/dbaux/api_test_automation/model/bench_config"
	restmodel "github.com/dbaux/api_test_automation/model/rest_config"
	"github.com/dbaux/api_test_automation/util"
	"github.com/educlos/testrail"

	"github.com/valyala/fasthttp"
)

type MyBench struct {
	requests                int64
	clients                 int
	url                     string
	urlsFilePath            string
	keepAlive               bool
	postDataFilePath        string
	writeTimeout            int
	readTimeout             int
	authHeader              string
	maxConnectionPerHost    int
	requestsPerClientPerSec int64
	resultFilePath          string
	ClientCountArr          []int
	MyCon                   *MyConn
}

/*var readThroughput int64
var writeThroughput int64*/

type MyConn struct {
	net.Conn
	readThroughput  int64
	writeThroughput int64
}

func (this *MyConn) Read(b []byte) (n int, err error) {
	len, err := this.Conn.Read(b)
	//fmt.Println("Read ", len, " bytes")
	if err == nil {
		atomic.AddInt64(&this.readThroughput, int64(len))
	}

	return len, err
}

func (this *MyConn) Write(b []byte) (n int, err error) {
	len, err := this.Conn.Write(b)
	//fmt.Println("Wrote ", len, " bytes")
	if err == nil {
		atomic.AddInt64(&this.writeThroughput, int64(len))
	}

	return len, err
}

func (bench *MyBench) InitVar(clientCountArr []int, initPeriodC int64, stepupPeriodC int64, maxPeriodC int64, requestsPerClientPerS int64, maxPerHost int, maxReadTimeout int, maxWriteTimeout int, resultFilePathC string) *MyBench {

	f := flag.NewFlagSet(os.Args[0], flag.ExitOnError)
	f.Int64Var(&bench.requestsPerClientPerSec, "r", 1, "Number of requests per client per sec")
	f.IntVar(&bench.clients, "c", 1, "Number of concurrent clients")
	f.StringVar(&bench.url, "u", "", "URL")
	f.StringVar(&bench.urlsFilePath, "f", "", "URL's file path (line seperated)")
	f.BoolVar(&bench.keepAlive, "k", true, "Do HTTP keep-alive")
	f.StringVar(&bench.postDataFilePath, "d", "", "HTTP POST data file path")
	f.IntVar(&bench.writeTimeout, "tw", maxWriteTimeout, "Write timeout (in milliseconds)")
	f.IntVar(&bench.readTimeout, "tr", maxReadTimeout, "Read timeout (in milliseconds)")
	f.StringVar(&bench.authHeader, "auth", "", "Authorization header")
	f.IntVar(&bench.maxConnectionPerHost, "mc", maxPerHost, "Max conn per host")
	f.StringVar(&bench.resultFilePath, "rfp", resultFilePathC, "Output result in a file")
	bench.ClientCountArr = clientCountArr
	return bench
}

func (bench *MyBench) PrintResults(result map[int]*benchmodel.BenchResult, startTime time.Time) {

	elapsed := int64(time.Since(startTime).Milliseconds())

	for i, v := range result {
		//fmt.Println(" index i-", i)
		fmt.Println("----------------------------------------------------------------")
		fmt.Printf("Api Id:%45s\n", v.ApiId)
		fmt.Printf("Response Status:%40d\n", v.ResponseStatus)
		fmt.Printf("Response Time:%40dms\n", v.ResponseTime)
		file := filepath.Base(bench.resultFilePath)
		fileName := "/tmp/scenario" + strconv.Itoa(i) + "_" + strings.Split(file, ".csv")[0]
		//fmt.Println("fileName-" + fileName)
		util.CreateEmptyFile(fileName)

		util.WriteToFile(fileName, v.Response)
		fmt.Printf("Response :%40s\n", fileName)
		fmt.Printf("Test Status:%40s\n", v.TestStatus)
		fmt.Printf("Failure Reason:%40s\n", v.FailureReason)
		fmt.Printf("Description:%40s\n", v.Description)
		val := fmt.Sprintf("%s,%d,%d,%s,%s,%s,%d,%s\n", v.ApiId, v.ResponseStatus, v.ResponseTime, fileName, v.TestStatus, v.FailureReason, elapsed, "\""+v.Description+"\"")
		util.WriteToFile(bench.resultFilePath, val)
		updateTestRailResult(v.TestRailId, v.FailureReason, v.TestStatus)
		fmt.Println("----------------------------------------------------------------")
	}
	fmt.Printf("Total Test time:                      %dms\n", elapsed)
}

func updateTestRailResult(testRailId int64, comment string, status string) {
	//fmt.Println("testRailId-", testRailId)
	if testRailId == 0 {
		return
	}

	username := os.Getenv("TESTRAIL_USERNAME")
	password := os.Getenv("TESTRAIL_TOKEN")
	//fmt.Println("username-", username, " password-", password)
	client := testrail.NewClient(benchmodel.TestRailURL, username, password)

	//fmt.Println("client-", client)
	//projectID := 1
	//suiteID := 4
	test, err := client.GetTest(int(testRailId))
	//fmt.Println("error-", err)
	if err == nil {
		//fmt.Println(test.ID)
		result := testrail.SendableResult{Comment: comment, StatusID: getStatusId(status)}
		addresult, err := client.AddResult(test.ID, result)
		fmt.Println("result-", addresult, " err-", err)
	}
}

func getStatusId(status string) int {
	switch status {
	case "Pass":
		return 1
	case "Fail":
		return 4
	}
	return 3
}

func readLines(path string) (lines []string, err error) {

	var file *os.File
	var part []byte
	var prefix bool

	if file, err = os.Open(path); err != nil {
		return
	}
	defer file.Close()

	reader := bufio.NewReader(file)
	buffer := bytes.NewBuffer(make([]byte, 0))
	for {
		if part, prefix, err = reader.ReadLine(); err != nil {
			break
		}
		buffer.Write(part)
		if !prefix {
			lines = append(lines, buffer.String())
			buffer.Reset()
		}
	}
	if err == io.EOF {
		err = nil
	}
	return
}

func (bench *MyBench) NewConfiguration() *benchmodel.BenchConfiguration {

	if bench.requestsPerClientPerSec <= 0 {
		fmt.Println("Requests or period must be provided")
		flag.Usage()
		os.Exit(1)
	}

	configuration := &benchmodel.BenchConfiguration{
		KeepAlive: bench.keepAlive,
		Requests:  int64((1 << 63) - 1),
	}

	configuration.Requests = bench.requestsPerClientPerSec
	configuration.MyClient.ReadTimeout = time.Duration(bench.readTimeout) * time.Millisecond
	configuration.MyClient.WriteTimeout = time.Duration(bench.writeTimeout) * time.Millisecond
	configuration.MyClient.MaxConnsPerHost = bench.maxConnectionPerHost

	configuration.MyClient.Dial = bench.MyDialer()

	return configuration
}

func setHeader(request *fasthttp.Request, headers restmodel.JsonParams) *fasthttp.Request {
	for k, v := range headers {
		//fmt.Printf(" setHeader key[%s] value[%s]\n", k, v)
		request.Header.Set(k, v.(string))
	}
	return request
}

func (bench *MyBench) MyDialer() func(address string) (conn net.Conn, err error) {
	return func(address string) (net.Conn, error) {
		conn, err := net.DialTimeout("tcp", address, 100*time.Second)

		if err != nil {
			return nil, err
		}

		bench.MyCon = &MyConn{Conn: conn}
		myConn := bench.MyCon

		return myConn, nil
	}
}

func (bench *MyBench) client(configuration *benchmodel.BenchConfiguration, allConfigureRest []restmodel.ConfigureRest, fastRest *fastrequest.FastRest) map[int]*benchmodel.BenchResult {
	//time.Sleep(500 * time.Millisecond)
	return fastRest.InitiateNextRequest(allConfigureRest[0], restmodel.ConfigureRest{}, allConfigureRest, configuration, []byte(""))
}

func (bench *MyBench) InitiateRequestAndBenchmark(allConfigureRest []restmodel.ConfigureRest, fastRest *fastrequest.FastRest, eventName string, mainWg *sync.WaitGroup, timeToReleaseConn int64) {
	flag.Parse()

	goMaxProcs := os.Getenv("GOMAXPROCS")

	if goMaxProcs == "" {
		runtime.GOMAXPROCS(runtime.NumCPU())
	}

	configuration := bench.NewConfiguration()
	//fmt.Printf("Dispatching %d clients\n", aclients, " initPeriod-", bench.initialPeriod)
	//results := make(map[int]*benchmodel.BenchResult)
	//startTime := time.Now()

	//result := &benchmodel.BenchResult{}
	//results[0] = result
	//result.EventName = eventName

	clonedAllConfigureRest := make([]restmodel.ConfigureRest, len(allConfigureRest))
	copy(clonedAllConfigureRest, allConfigureRest)
	//fmt.Println("clonedAllConfigureRest-", len(clonedAllConfigureRest), " allConfigureRest-", len(allConfigureRest))
	time := time.Now()
	results := bench.client(configuration, clonedAllConfigureRest, fastRest)
	bench.PrintResults(results, time)
	//fmt.Println("Waiting for results...")

	mainWg.Done()
}
