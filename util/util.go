package util

import (
	"bufio"
	"bytes"
	"encoding/binary"
	"os"
)

//Check ... if error exit
func Check(e error) {
	if e != nil {
		panic(e)
	}
}

func CreateFileWithHeader(fileName string) {
	fileHandle, err := os.Create(fileName)
	//fmt.Printf("creating new file \n")
	Check(err)

	w := bufio.NewWriter(fileHandle)
	//Write headers
	w.WriteString("ApiId,ResponseStatus,Response Time,Response,Test Status,Failure Reason,Test time\n")
	//fmt.Printf("wrote %d bytes\n", n)
	w.Flush()
}

func CreateEmptyFile(fileName string) {
	fileHandle, err := os.Create(fileName)
	//fmt.Printf("creating new file \n")
	Check(err)

	w := bufio.NewWriter(fileHandle)
	//Write headers
	w.WriteString("")
	//fmt.Printf("wrote %d bytes\n", n)
	w.Flush()
}

func WriteToFile(fileName string, data string) {
	var fileHandle *os.File
	var err error

	if _, err := os.Stat(fileName); err == nil {
		// path/to/whatever exists
		//fileHandle, err = os.Open(fileName)
		fileHandle, err = os.OpenFile(fileName, os.O_APPEND|os.O_WRONLY, os.ModeAppend)
		//fmt.Printf("file exists open\n")
	} else if os.IsNotExist(err) {
		// path/to/whatever does *not* exist
		fileHandle, err = os.Create(fileName)
		//fmt.Printf("file does not exist create \n")
		Check(err)
	} else {
		fileHandle, err = os.Create(fileName)
		//fmt.Printf("file does not exist create\n")
		Check(err)
	}

	w := bufio.NewWriter(fileHandle)
	w.WriteString(data)
	//fmt.Printf("wrote %d bytes\n", n)
	w.Flush()
	Check(err)
}

func Float64frombytes(byteData []byte) float64 {
	buf := bytes.NewBuffer(byteData) // b is []byte
	myfirstint, _ := binary.ReadVarint(buf)
	return float64(myfirstint)
}
