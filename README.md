# README #



### What is this repository for? ###

This tool is developed for api testing. It can be used to configure multiple api test cases.

### How do I get set up? ###

Note: Minimum version of go required - 1.13

1) git clone git@bitbucket.org:parkjockey/api_test_automation_tool.git
2) cd api_test_automation_tool
3) go build
4) ./api_test_automation

For configuring apis, follow confluence page -

https://reeftechnology.atlassian.net/wiki/spaces/REEF/pages/291242153/DBAUX+Api+Testing+Tools


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin

