package main

import (
	"fmt"
	"os"
	"testing"

	"github.com/dbaux/api_test_automation/model"
	"github.com/dbaux/api_test_automation/params"
	"github.com/dbaux/api_test_automation/parser"
	"github.com/dbaux/api_test_automation/testrest"
	//"net/http/httptest"
)

func TestApi(t *testing.T) {
	//intialize 'var' placeholder map for global variables

	var configParser parser.Parser = parser.ConfigParser{params.TestFileName}
	var configRest []model.ConfigureRest = configParser.ParseConfiguration(params.TestFileName)
	testRest := testrest.TestRest{configParser}
	testRest.InitiateNextRequest(t, configRest[0], model.ConfigureRest{}, configRest)
	fmt.Fprintln(os.Stdout, "Ending execution")
}
